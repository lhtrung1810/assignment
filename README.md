This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Configure environment variable at:
`project-folder/.env`

### In the project directory, run:
`npm install`   
`npm start`

Runs the app in the development mode.   
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Builds the app for production to the `build` folder
`npm run build`

### Run API server to get sample data
`node server.js`
