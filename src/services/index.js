import axios from 'axios';

export const getData = async () => {
  try {
    const data = await axios.get('/getData').then((resp) => resp.data);
    return data;
  } catch (error) {
    throw error;
  }
}

export const saveData = async (body) => {
  try {
    const data = await axios.post('/saveData', body).then((resp) => resp.data);
    return data;
  } catch (error) {
    throw error;
  }
}
