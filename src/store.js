import Immutable from 'immutable';
import {
  createStore,
  applyMiddleware,
} from 'redux';
import {
  createLogger
} from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers';

const store = createStore(
  reducers,
  applyMiddleware(thunk, createLogger({
    stateTransformer: state => //state
    {
      const newState = {};
      for (let i of Object.keys(state)) {
        if (Immutable.Iterable.isIterable(state[i])) {
          newState[i] = state[i].toJS();
        } else {
          newState[i] = state[i];
        }
      }
      return newState;
    }
  })),
);

export default store;
