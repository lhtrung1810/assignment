import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';

class PlatformPosition extends Component {

  constructor(props) {
    super(props);
    this.validPositions = this.validPositions.bind(this);
    this.toggleAll = this.toggleAll.bind(this);
    this.itemChange = this.itemChange.bind(this);
    const state = this.propsToState(props);
    this.state = {
      ...state,
      visible: true,
    };
  }

  propsToState(props) {
    const validPositions = _.filter(props.data['list_items'], this.validPositions(props));
    return { validPositions };
  }

  validPositions(props) {
    return (pos) => {
      const { data, devices } = props;
      const result = data['result'];
      for(var i in result[pos].device) {
        const itemDevice = result[pos].device[i];
        if(devices.indexOf(itemDevice) !== -1) {
          return true;
        }
      }
      return false;
    }
  }

  toggleAll() {
    const checked = !this.props.data['user_selection'].length;
    let selection = [];
    if(checked) {
      selection = this.state.validPositions;
    }
    this.props.onChange({
      type: 'update',
      selection
    });
  }

  itemChange(e) {
    const target = e.target;
    let selection = this.props.data['user_selection'];
    if(target.checked) {
      selection.push(target.value);
    } else {
      _.remove(selection, item => item === target.value);
    }
    this.props.onChange({
      type: 'update',
      selection
    });
  }

  togleSelection() {
    this.setState({
      visible: !this.state.visible
    });
  }

  componentWillReceiveProps(props) {
    const state = this.propsToState(props);
    this.setState({...state})
  }

  postRender() {
    const { data } = this.props;
    if(this.toggleAllInput) {
      this.toggleAllInput.indeterminate = data['user_selection'].length && data['user_selection'].length < data['list_items'].length;
    }
  }

  componentDidMount() {
    this.postRender();
  }

  componentDidUpdate() {
    this.postRender();
  }

  render() {
    const { validPositions } = this.state;
    const { data, required } = this.props;
    const listItem = data['list_items'];
    const result = data['result'];
    const userSelection = data['user_selection'];

    let listClassName = 'list-group positions';
    let iconClassName = 'fa fa-caret-up';
    if(this.state.visible) {
      iconClassName = 'fa fa-caret-down';
      listClassName += ' show';
    }
    const positionList = listItem.map(key => {
      let checkbox = <i className="fa fa-ban" />;
      if(validPositions.indexOf(key) !== -1) {
        const checkboxAttrs = {
          type: 'checkbox',
          className: 'form-check-input',
          checked: userSelection.indexOf(key) !== -1,
          onChange: this.itemChange,
          value: key,
          name: 'item',
        };

        if(required && userSelection.length === 1 && checkboxAttrs.checked) {
          checkboxAttrs.disabled = 'disabled';
        }

        checkbox = <input {...checkboxAttrs} />
      }
      return <li key={'pos-item-' + key} className="list-group-item d-flex justify-content-between align-items-center">
        {result[key].label}
        <small>{checkbox}</small>
      </li>
    });

    const checkAllAttrs = {
      type: 'checkbox',
      className: 'form-check-input',
      checked: validPositions.length === userSelection.length,
      ref: toggleAllInput => this.toggleAllInput = toggleAllInput,
      onChange: this.toggleAll,
    };
    if(required) checkAllAttrs.disabled = 'disabled'

    return   (
      <form
        ref={form => this.form = form}
      >
        <div className="d-flex w-100 justify-content-between">
          <span style={{position: 'absolute'}} onClick={this.togleSelection.bind(this)}>
            <i className={iconClassName}></i>
          </span>
          <h5 style={{paddingLeft: 20}} className="mb-1">{data.label}</h5>
          <small>
            {
              validPositions.length ? <input {...checkAllAttrs} /> : null
            }
          </small>
        </div>
        <ul className={listClassName}>{positionList}</ul>
      </form>
    );
  }
}

PlatformPosition.propTypes = {
  data: PropTypes.object,
  devices: PropTypes.array,
  selection: PropTypes.array,
  onChange: PropTypes.func
}

export default PlatformPosition;
