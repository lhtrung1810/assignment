import React, { Component } from 'react';
import './App.css';
import {
  Provider
} from 'react-redux';
import './App.css';
import Main from './containers/main.container';
import store from './store';

class App extends Component {
  render() {
    return   ( <
      Provider store = {
        store
      } >
      <
      Main / >
      <
      /Provider>
    );
  }
}

export default App;
