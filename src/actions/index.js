import * as constants from './../constants';
import * as services from './../services';

export const getDataRequest = () => async dispatch => {
  dispatch({
    type: constants.DATA_GET_REQUEST,
  });

  try {
    const data = await services.getData();
    dispatch(getDataSuccess(data.placement));
  } catch(error) {
    dispatch(getDataFailed(error));
  }
};

export const getDataSuccess = payload => dispatch => dispatch({
  type: constants.DATA_GET_SUCCESS,
  payload
})

export const getDataFailed = payload => dispatch => dispatch({
  type: constants.DATA_GET_FAILED,
  payload
})


export const selectDevice = payload => dispatch => dispatch({
  type: constants.USER_CHANGE_DEVICE,
  payload
});

export const updatePosition = payload => dispatch => dispatch({
  type: constants.USER_CHANGE_POSITION,
  payload
});
