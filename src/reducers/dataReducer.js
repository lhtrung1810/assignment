import {
  fromJS
} from 'immutable';
import {
  handleActions
} from 'redux-actions';

import {
  DATA_GET_REQUEST,
  DATA_GET_SUCCESS,
  DATA_GET_FAILED,
  USER_CHANGE_DEVICE,
  USER_CHANGE_POSITION,
} from './../constants';

const initialState = fromJS({});

const setDefaultValue = (data, key) => {
  const value = data.get(key);
  const userSelection = value.get('user_selection');
  const defaultSelection = value.get('default_selection');
  const listItems = value.get('result').keySeq().toList();
  let selection;
  if((!userSelection || !userSelection.size) && (!defaultSelection || !defaultSelection.size)) {
    selection = key === 'publisher_platforms' ? fromJS(['facebook_positions']) : listItems;
  } else if(!userSelection || !userSelection.size) {
    selection = defaultSelection;
  } else {
    selection = userSelection
  }
  return value.set('user_selection', selection).set('list_items', listItems);
}

const getValidPostions = (state, platform, devices) => {
  const data =  state.get('userData');
  const positions = data.getIn([platform, 'result']);
  // conver device to string to compoare
  const device = devices.toJS().sort().join(',');
  let validPositions = fromJS([]);
  positions.mapEntries(entries => {
    const pos = entries[1];
    const keyPos = entries[0];
    const posDevice = pos.get('device').toJS().sort().join(',');
    if(device === posDevice || device.indexOf(posDevice) !== -1 || posDevice.indexOf(device) !== -1) {
      validPositions = validPositions.push(keyPos);
    }
  });
  return validPositions;
}

const dataReducer = handleActions({
  [DATA_GET_REQUEST]:  (state, e) => state.set('sending', true),
  [DATA_GET_SUCCESS]: (state, e) => {
    const payload = fromJS(e.payload);
    const validation = payload.get('validation');
    const initData = payload.get('data');
    const publisherPlatforms = setDefaultValue(initData, 'publisher_platforms');
    let data = initData.set('publisher_platforms', publisherPlatforms);
    data = data.map((value, key) => {
      if(key === 'publisher_platforms') return value;
      let keyData = setDefaultValue(data, key);
      if(key !== 'device_platforms' && !data.getIn(['publisher_platforms', 'user_selection']).includes(key)) {
        keyData = keyData.set('user_selection', fromJS([]));
      }
      return keyData;
    });
    return state
    .set('initData', initData)
    .set('userData', data)
    .set('validation', validation)
    .set('sending', false)
  },
  [DATA_GET_FAILED]: (state, e) => state.set('error', e.payload).set('sending', false),
  [USER_CHANGE_DEVICE]: (state, e) => {
    const publisherPlatformPath = ['userData', 'publisher_platforms', 'user_selection'];
    const devicePlatformPath = ['userData', 'device_platforms', 'user_selection'];
    const devices = fromJS(e.payload);
    let newState = state;
    let platforms = state.getIn(publisherPlatformPath);
    // validate user's selection based on device
    platforms.forEach(platform => {
      const userSelectionPath = ['userData', platform, 'user_selection'];
      const validPositions = getValidPostions(state, platform, devices);
      let userSelection = state.getIn(userSelectionPath);
      userSelection = userSelection.filter(
        value => validPositions.includes(value)
      );


      if(!userSelection.size) {
        newState = newState.setIn(
          publisherPlatformPath,
          platforms.delete(
            platforms.indexOf(platform)
          )
        );
      }

      newState = newState.setIn(userSelectionPath, userSelection);

    });
    // if there is no valid publisher platform, set facebook positions as default
    if(!newState.getIn(publisherPlatformPath).size) {
      newState = newState
      .setIn(
        publisherPlatformPath, fromJS(['facebook_positions'])
      )
      .setIn(
        ['userData', 'facebook_positions', 'user_selection'],
        getValidPostions(state, 'facebook_positions', devices)
      );
    }
    return newState.setIn(devicePlatformPath, devices);
  },
  [USER_CHANGE_POSITION]: (state, e) => {
    const { platform, selection } = e.payload;

    const platformSelection = ['userData', 'publisher_platforms', 'user_selection'];
    const deviceSelection = ['userData', platform, 'user_selection']

    const newSelection = fromJS(selection);
    let newState = state
    .setIn(
      deviceSelection,
      newSelection
    );

    let platforms = newState.getIn(platformSelection);
    if(!platforms.includes(platform) && newSelection.size) {
      platforms = platforms.push(platform);
    }

    if(!newSelection.size) {
      platforms = platforms.delete(
        platforms.indexOf(platform)
      );
    }

    return newState.setIn(platformSelection, platforms);
  }
}, initialState);

export default dataReducer;
