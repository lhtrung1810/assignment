import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDataRequest, selectDevice } from './../actions';
import { saveData } from './../services';
import _ from 'lodash';

import  PlatformPlacement from './platform-placement.container';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {}
    }
    this.userSelection = {};
    this.selectedDevicePlatforms = [];
    this.deviceChange = this.deviceChange.bind(this);
    this.props.getData();

  }

  async save() {
    try {
      await saveData(this.state.userSelectionData);
      alert('Data saved!');
    } catch(e) {
      alert(e);
    }
  }

  reset() {
    this.props.getData();
  }

  deviceChange(e) {
    this.setState({
      selectedDevice: e.target.value
    });
    let devices;
    if(!e.target.value) {
      devices = this.props.data.getIn(['device_platforms', 'list_items']).toJS();
    } else {
      devices = e.target.value.split(',')
    }
    this.props.selectDevice(devices);
  }

  onPostionChange(platform) {
    return (e) => {
      const { data } = e;
      this.userSelection[platform] = { data };
      this.userSelection.devicePlatforms = this.selectedDevicePlatforms;
      this.props.updateSelection({ devicePlatforms: this.selectedDevicePlatforms, platform, data });
    }
  }

  getLabel(key) {
    const keyArray = key.split('positions_');
    const platformLabel = this.props.data.getIn([keyArray[0] + 'positions', 'label']);
    const positionLabel = this.props.data.getIn([keyArray[0] + 'positions', 'result', keyArray[1], 'label']);
    return `${platformLabel} ${positionLabel}`;
  }

  componentWillReceiveProps(props) {
    if(props.data) {
      const { data } =  props;
      /** get device_platforms **/
      const devicePlatforms = data.getIn(['device_platforms', 'result']);
      const listDevice = data.getIn(['device_platforms', 'list_items']);

      const device = listDevice.sort().join(',');
      let selectedDevice = data.getIn(['device_platforms', 'user_selection']).sort().join(',');
      selectedDevice = device === selectedDevice ? '' : selectedDevice;

      /** publisher_platforms **/
      const publisherPlatforms =  data.getIn(['publisher_platforms', 'list_items']);
      /** set user's selection data **/
      const userSelectionData = {};
      let allSelectionData = [];
      data.forEach((value, key) => {
        userSelectionData[key] = value.get('user_selection').toJS();
        if(userSelectionData[key].length && publisherPlatforms.includes(key)) {
          allSelectionData = allSelectionData.concat(
            _.map(userSelectionData[key], selection => `${key}_${selection}`)
          )
        }
      });
      /** validate user's selection data **/
      let errors = {};
      props.validation.forEach(validation => {
        const required = validation.get('required') ? validation.get('required').toJS() : false;
        if(required) {
          _.forEach(required, (requiredData, selected) => {
            const missingData = _.difference(requiredData, allSelectionData);
            if(allSelectionData.indexOf(selected) !== -1 && missingData.length) {
              const selectedLabel = this.getLabel(selected);
              const itemErrors = [];
              missingData.forEach(item => {
                const itemLabel = this.getLabel(item);
                const error = `To use ${selectedLabel}, please select ${itemLabel} placements`;
                itemErrors.push(error);
              });
              errors[selected] = itemErrors;
            }
          });
        }
      });

      this.setState({
        hasData: true,
        devicePlatforms,
        listDevice,
        selectedDevice,
        publisherPlatforms,
        data,
        userSelectionData,
        errors
      });
    }
  }

  renderView() {
    const { listDevice, devicePlatforms, publisherPlatforms, errors } = this.state;
    const platformList = listDevice.map(device => {
        return <option key={'option-' + device} value={device}>{devicePlatforms.get(device)}</option>
    });

    const disableSaving = {};
    if(Object.keys(errors).length) {
      disableSaving.disabled = 'disabled';
    }

    return (
      <div>
        <div className="form-group">
          <label><strong>Device types</strong></label>
          <select className="form-control" value={this.state.selectedDevice} onChange={this.deviceChange} >
            <option value="">All devices (Recommended)</option>
            {
              platformList
            }
          </select>
        </div>
        <div className="form-group">
          <label><strong>Platforms</strong></label>
            <div className="list-group">
              {
                publisherPlatforms.map(
                  platform => <PlatformPlacement
                    platform={platform}
                    key={'position-' + platform}
                    className="list-group-item list-group-item-action flex-column align-items-start" />
                  )
              }
            </div>
          </div>
          <div className="form-group">
            <button className="btn btn-primary" {...disableSaving} onClick={this.save.bind(this)}>Save</button>
            <button className="btn btn-default" style={{marginLeft: 10}} onClick={this.reset.bind(this)}>Reset</button>
        </div>
      </div>
    );
  }

  renderError() {
    const { errors } = this.state;
    const errorArray = Object.keys(errors);
    return (
      errorArray.length ?
      <div className="alert alert-danger" role="alert">
        {
           errorArray.map(item => {
            return errors[item].map((error, index) => {
              return <p key={`${item}-${index}`}>
                {error}
              </p>
            })
          })
        }
      </div>
      : null
    )
  }

  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col-12">
            <h3>Assignment</h3>
          </div>
          <div className="col-12 d-block d-md-none">
            {this.renderError()}
          </div>
          <div className="col-md-6">
            {
              this.state.hasData ? this.renderView() : null
            }
            </div>
            <div className="col-md-6 d-none d-md-block" style={{marginTop: 32}}>
              {this.renderError()}
            </div>
          </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.data.get('userData'),
    validation: state.data.get('validation'),
  }
};

const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(getDataRequest()),
  selectDevice: data => dispatch(selectDevice(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
