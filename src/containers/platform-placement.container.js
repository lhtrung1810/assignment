import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updatePosition } from './../actions';
import PlatformPosition from './../components/PlatformPosition';


class PlatformPlacement extends React.Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    const { selection } = value;
    this.props.updatePosition({
      platform: this.props.platform,
      selection
    });
  }

  render() {
    const { className, data, devices, platforms, platform } = this.props;

    const required = platforms && platforms.size === 1 && platforms.get(0) === platform;

    return (
      <div className={className}>
      <PlatformPosition
        data={data.toJS()}
        devices={devices.toJS()}
        onChange={this.onChange}
        required={required}
      />
      </div>
    )
  }
}

const mapStateToProps = ((state, props) => {
  return {
    data: state.data.getIn(['userData', props.platform]),
    devices: state.data.getIn(['userData', 'device_platforms', 'user_selection']),
    platforms: state.data.getIn(['userData', 'publisher_platforms', 'user_selection']),
  }
});

const mapDispatchToProps = (dispatch) => ({
  updatePosition: (data) => dispatch(updatePosition(data))
})

PlatformPlacement.propTypes = {
  platform: PropTypes.string,
  platforms: PropTypes.any,
  dispatch: PropTypes.func,
  data: PropTypes.any,
  devices: PropTypes.any,
}

export default connect(mapStateToProps, mapDispatchToProps)(PlatformPlacement);
