const express = require('express')
const app = express();
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const NODE_ENV = process.env.NODE_ENV || 'production';
const PORT = process.env.PORT || 4000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(require('cors')());

if(NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, 'build')));
}

app.get('/api/getData', (req, res) => {
  const data = require('./data.json');
  return res.json(data);
});

app.post('/api/saveData', (req, res) => {
  return res.json(req.body);
});

app.get('*', (req, res) => {
  if(NODE_ENV === 'production') {
    return res.sendFile(__dirname + '/build/index.html');
  }
  return res.send('Development mode');
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}`));
